"""Event handlers."""
import copy

from cki_lib.logger import get_logger
import cki_lib.misc
import pipeline_tools.utils as utils


LOGGER = get_logger(__name__)

EXTERNAL_LABEL_NAME = 'External Contribution'
EXTERNAL_LABEL_DESCRIPTION = 'MR submitted by an external contributor'
EXTERNAL_LABEL_COLOR = '#FF0000'


EMOJI_MAP = {
    'created': ':hourglass_flowing_sand:',
    'pending': ':hourglass_flowing_sand:',
    'running': ':hourglass_flowing_sand:',
    'canceled': ':grey_exclamation:',
    'skipped': ':grey_exclamation:',
    'success': ':heavy_check_mark:',
    'failed': ':sob:',
    'manual': ':no_entry:'
}


WELCOME_MESSAGE = """Hi! This is the friendly CKI test bot.

It appears that you are not a member of {group}. This means that the CI
pipeline on your MR will fail. As getting testing is important, I'll be
responsible for testing of your changes. After every MR change, I'll start a
small testing pipeline and link it here so you can follow the results. I'll
also create and link a pipeline for hardware testing that the reviewers can
start to get extra test coverage."""


PIPELINE_MESSAGE = """Testing pipeline status:

Basic testing pipeline:

&nbsp;&nbsp;{external_link} - {external_status} {external_emoji}

Full testing pipeline:

&nbsp;&nbsp;{trusted_link} - {trusted_status} {trusted_emoji}

*If the full pipeline is not running, wait for a reviewer to trigger it.*
"""


def already_commented(mr, bot_name):
    """Check if bot_name user already added a welcome comment on the MR.

    Returns:
        True if a first comment of a discussion was added by the bot
        False if there is no first discussion comment added by the bot
    """
    for discussion in mr.discussions.list(as_list=False):
        if discussion.attributes['notes'][0]['author']['username'] == bot_name:
            return True

    return False


def ensure_label_exists(project):
    """Check if a label for external contributions exists and if not, create
    it.
    """
    for label in project.labels.list(as_list=False):
        if label.attributes['name'] == EXTERNAL_LABEL_NAME:
            return

    # Label doesn't exist, create it.
    LOGGER.info('Creating new label for %s',
                project.attributes['path_with_namespace'])
    label = project.labels.create({'name': EXTERNAL_LABEL_NAME,
                                   'color': EXTERNAL_LABEL_COLOR,
                                   'description': EXTERNAL_LABEL_DESCRIPTION})


def get_vars_from_hook(var_list):
    """Get a dictionary of variables from pipeline webhook. The webhook data
    contains a list of dictionaries which is less fun to deal with.
    """
    variables = {}
    for var_dict in var_list:
        variables[var_dict['key']] = var_dict['value']

    return variables


def handle_mr(gitlab_instance, project_config, mr_data):
    """Trigger pipelines for given MR."""
    # Get extra data
    extra_variables = {
        'git_url': mr_data['object_attributes']['source']['git_http_url'],
        'branch': mr_data['object_attributes']['source_branch'],
        'target_repo': mr_data['object_attributes']['target']['git_http_url'],
        'target_branch': mr_data['object_attributes']['target_branch'],
        'child_pipeline': 'True',
        'name': '{}-{}'.format(mr_data['project']['path_with_namespace'],
                               mr_data['object_attributes']['iid']),
        'title': '{}: MR {}'.format(mr_data['project']['path_with_namespace'],
                                    mr_data['object_attributes']['iid'])
    }

    project = gitlab_instance.projects.get(project_config['.mr_project'])
    mr = project.mergerequests.get(mr_data['object_attributes']['iid'])
    if not already_commented(mr, gitlab_instance.user.username):
        mr.discussions.create(
            {'body': WELCOME_MESSAGE.format(group=project_config['group'])}
        )

        # If this is the first time we see the MR that means there is no label
        # assigned to it yet.
        ensure_label_exists(project)
        mr.labels.append(EXTERNAL_LABEL_NAME)
        mr.save()

    trigger = copy.deepcopy(project_config)
    trigger.update(extra_variables)
    trigger = utils.clean_config(trigger)

    # Trigger external pipeline
    trigger['cki_pipeline_branch'] = project_config['.cki_external_pipeline_branch']  # noqa
    trigger['cki_project'] = project_config['.cki_external_pipeline_project']
    trigger['cki_pipeline_type'] = 'external'
    external_token = cki_lib.misc.get_env_var_or_raise(
        project_config['.external_trigger_token_name']
    )
    external_pipeline = utils.safe_trigger(
        gitlab_instance, external_token, [trigger]
    )[0]

    # Trigger trusted pipeline
    trigger['cki_pipeline_branch'] = project_config['.cki_trusted_pipeline_branch']  # noqa
    trigger['cki_project'] = project_config['.cki_trusted_pipeline_project']
    trigger['cki_pipeline_type'] = 'trusted'
    trigger['manual_trigger'] = 'True'
    trusted_token = cki_lib.misc.get_env_var_or_raise(
        project_config['.trusted_trigger_token_name']
    )
    trusted_pipeline = utils.safe_trigger(
        gitlab_instance, trusted_token, [trigger]
    )[0]

    # Create a status message with initial values
    status_message = PIPELINE_MESSAGE.format(
        external_link=external_pipeline.attributes['web_url'],
        external_status='created',
        external_emoji=EMOJI_MAP['created'],
        trusted_link=trusted_pipeline.attributes['web_url'],
        trusted_status='manual',
        trusted_emoji=EMOJI_MAP['manual']
    )
    mr.discussions.create({'body': status_message})


def update_pipeline_data(config, gitlab_instance, webhook_data):
    """Update existing MR with current pipeline status."""
    pipeline_id = str(webhook_data['object_attributes']['id'])
    pipeline_vars = get_vars_from_hook(
        webhook_data['object_attributes']['variables']
    )
    # Handle project names with '-'
    project_path, mr_id = pipeline_vars['name'].rsplit('-', maxsplit=1)

    # Find the correct project
    for project_config in config.values():
        if project_path == project_config['.mr_project']:
            project = gitlab_instance.projects.get(project_path)
            break
    else:
        # Something went seriously wrong here but let's not crash the handler
        # just for fun ok?
        LOGGER.error('Unable to find matching MR project for %s',
                     webhook_data['commit']['url'])
        return

    # Find the correct comment to edit. Bot only creates new threads so we
    # don't need to dig through every comment in discussions and it's enough to
    # check the top comments.
    mr = project.mergerequests.get(mr_id)
    bot_name = gitlab_instance.user.username
    for discussion in mr.discussions.list(as_list=False):
        first_comment_data = discussion.attributes['notes'][0]
        if first_comment_data['author']['username'] == bot_name and \
                pipeline_id in first_comment_data['body']:
            comment = discussion.notes.get(first_comment_data['id'])
            break
    else:
        # Did someone delete our original comment?
        LOGGER.error('Unable to find comment for %s in MR %s from %s',
                     webhook_data['commit']['url'],
                     mr_id,
                     project_path)
        return

    # Edit and update the comment
    body_lines = comment.body.splitlines()
    for index, line in enumerate(body_lines):
        if pipeline_id in line:
            status = webhook_data['object_attributes']['status']
            new_line = line.rsplit('-', maxsplit=1)[0] + ' - {} {}'.format(
                status, EMOJI_MAP[status]
            )
            body_lines[index] = new_line
            break
    comment.body = '\n'.join(body_lines)
    comment.save()
