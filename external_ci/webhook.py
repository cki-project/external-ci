import json
import logging
import os
import sys
from yaml import load, Loader

import cki_lib.misc
import falcon
import gitlab
from pipeline_tools.utils import process_config_tree
import sentry_sdk
from sentry_sdk.integrations.falcon import FalconIntegration

from . import handlers


def load_config(config_path):
    """Read the config yaml and parse out actual definitions."""
    with open(config_path) as config_file:
        unparsed_config = load(config_file, Loader=Loader)

    return process_config_tree(unparsed_config)


def get_project_config(all_configs, project_name):
    """Get the configuration for given project."""
    for project_config in all_configs.values():
        if project_config['.mr_project'] == project_name:
            return project_config

    raise NotImplementedError(f'Missing config for {project_name}')


def get_mr_data(gitlab_instance, project_config, mr_id):
    """Retrieve data mimicking MR webhooks so we can unify handlers for
    production and manual triggers.
    """
    project = gitlab_instance.projects.get(project_config['.mr_project'])
    mr = project.mergerequests.get(mr_id)

    # Source URL is not available in MR data so we have to do an extra project
    # query to get it
    source_project = gitlab_instance.projects.get(
        mr.attributes['source_project_id']
    )
    source_url = source_project.attributes['http_url_to_repo']

    return {
        'object_attributes': {
            'source': {'git_http_url': source_url},
            'target': {'git_http_url': project.attributes['http_url_to_repo']},
            'iid': mr_id,
            'source_branch': mr.attributes['source_branch'],
            'target_branch': mr.attributes['target_branch'],
        },
        'project': {'name': project.attributes['name']}
    }


class GitlabWebhook:
    def __init__(self, config, gitlab_instance):
        self.config = config
        self.gitlab = gitlab_instance

    def on_get(self, request, response):
        """Is the hook alive?"""
        response.body = 'All good here!'
        response.status = falcon.HTTP_200

    def on_post(self, request, response):
        """POST request handler."""
        webhook_data = json.load(request.bounded_stream)

        # MR was submitted or modified
        if webhook_data['object_kind'] == 'merge_request':
            project_name = webhook_data['project']['path_with_namespace']
            author = webhook_data['user']['username']

            project_config = get_project_config(self.config, project_name)
            group = self.gitlab.groups.get(project_config['.group'])
            for member in group.members.all(as_list=False, all=True):
                if member.attributes['username'] == author:
                    logging.debug('Found internal contributor, not triggering')
                    response.status = falcon.HTTP_403
                    return

            # If we got this far we should trigger the pipelines
            handlers.handle_mr(self.gitlab,
                               project_config,
                               webhook_data)

        # Pipeline for MR changed status
        elif webhook_data['object_kind'] == 'pipeline':
            handlers.update_pipeline_data(self.config,
                                          self.gitlab,
                                          webhook_data)
        else:
            logging.info('Unknown hook type: %s', webhook_data['object_kind'])

        response.status = falcon.HTTP_200


class ManualHook:
    """Manually trigger the pipelines for MR. Nothing else."""
    def __init__(self, config, gitlab_instance):
        self.config = config
        self.gitlab = gitlab_instance

    def on_post(self, request, response):
        """POST request handler."""
        mr_id = int(request.get_param('mr_id', required=True))
        project_name = request.get_param('project', required=True)
        project_config = get_project_config(self, project_name)

        handlers.handle_mr(self.gitlab,
                           project_config,
                           get_mr_data(self.gitlab, project_config, mr_id))
        response.status = falcon.HTTP_200


# Get all the configs
if cki_lib.misc.is_production():
    sentry_sdk.init(ca_certs=os.getenv('REQUESTS_CA_BUNDLE'),
                    integrations=[FalconIntegration()])

logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s',
                    level=logging.INFO,
                    stream=sys.stdout)
logging.getLogger('requests').setLevel(logging.WARNING)
logging.getLogger("urllib3").setLevel(logging.WARNING)

ci_config = load_config(cki_lib.misc.get_env_var_or_raise('CONFIG_PATH'))

glab = gitlab.Gitlab(
    cki_lib.misc.get_env_var_or_raise('GITLAB_URL'),
    private_token=cki_lib.misc.get_env_var_or_raise('GITLAB_PRIVATE_TOKEN'),
    api_version=4
)
glab.auth()

# Initialize the actual webhook
API = application = falcon.API()
API.req_options.auto_parse_form_urlencoded = True

webhook_receiver = GitlabWebhook(ci_config, glab)
API.add_route('/', webhook_receiver)

# Allow turning off manual requests if e.g. the webhook is running in untrusted
# environment
if cki_lib.misc.get_env_bool('ENABLE_MANUAL', True):
    manual_hook = ManualHook(ci_config, glab)
    API.add_route('/manual', manual_hook)
