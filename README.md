# external-ci

Webhook for extrenal CI on GitLab merge requests.

# How to run it

Get the container from `registry.gitlab.com/cki-project/external-ci/external-ci:latest`
and run it. Make sure the deployment has access to all needed environment variables
mentioned in the sections below.

If you need a local deployment, export all environment varibales mentioned in the
section below and run

```
gunicorn --bind 0.0.0.0 --reload -k sync external_ci.webhook --timeout 120 --workers 5
```

# Environment variables

Following variables are required:

* `GITLAB_URL`: URL of the GitLab instance we're working on.
* `GITLAB_PRIVATE_TOKEN`: Private token of the bot account to use. Needs to have
  permissions to trigger pipelines in configured pipeline projects.
* `CONFIG_PATH`: Path to the project configuration to use.
* Trigger tokens for trusted and external pipelines. Variable names are configurable
  under `.external_trigger_token_name` and `.trusted_trigger_token_name` settings.
* `IS_PRODUCTION`: `True` if production pipelines should be triggered. Retriggers
  (testing pipelines) are created if the variable is not truthy value.

Following variables are optional:

* `IS_PRODUCTION`: Defaults to `False` if not present. If `True`, sentry integration
  is set up and an additional `SENTRY_DSN` variable is required.
* `ENABLE_MANUAL`: Defaults to `True` if not present. If `True`, a `/manual` endpoint
  is enabled to manually trigger pipelines for a specific MR.


# Configuration format and example

Multiple projects per single webhook instance are supported.

Example of two project configurations sharing the pipeline projects:

```
.pipeline_configs:
  .cki_trusted_pipeline_project: <path_with_namespace>
  .cki_trusted_pipeline_branch: <branch_name>
  .trusted_trigger_token_name: <variable_name>
  .cki_external_pipeline_project: <path_with_namespace>
  .cki_external_pipeline_branch: <branch_name>
  .external_trigger_token_name: <variable_name>

project_name:
  .extends: .pipeline_configs
  .mr_project: <path_with_namespace>
  .group: <group>

project_name2:
  .extends: .pipeline_configs
  .mr_project: <path_with_namespace>
  .group: <group>
```

Example of two project configurations *not* sharing the pipeline projects:

```
project_name:
  .cki_trusted_pipeline_project: <path_with_namespace>
  .cki_trusted_pipeline_branch: <branch_name>
  .trusted_trigger_token_name: <variable_name>
  .cki_external_pipeline_project: <path_with_namespace>
  .cki_external_pipeline_branch: <branch_name>
  .external_trigger_token_name: <variable_name>
  .mr_project: <path_with_namespace>
  .group: <group>

project_name2:
  .cki_trusted_pipeline_project: <path_with_namespace>
  .cki_trusted_pipeline_branch: <branch_name>
  .trusted_trigger_token_name: <variable_name>
  .cki_external_pipeline_project: <path_with_namespace>
  .cki_external_pipeline_branch: <branch_name>
  .external_trigger_token_name: <variable_name>
  .mr_project: <path_with_namespace>
  .group: <group>
```

Add any variables required by the pipelines into either the project-specific or
common settings depending on your needs.
