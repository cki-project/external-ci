#!/bin/bash

gunicorn --bind 0.0.0.0 --reload -k sync external_ci.webhook --timeout 120 --workers 5 2>&1 | \
    tee -a "/logs/external_ci.log"
